package challange_java_ade;

import java.util.Scanner;

public class Luas
{
    public static void persegi(){
        Scanner input = new Scanner(System.in);
        System.out.println("Silahkan masukkan sisi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegi();
            return;
        }
        double sisi = input.nextDouble();
        double luasPersegi;
        luasPersegi = sisi*sisi;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas Persegi = %.2f               |\n",luasPersegi);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();

    }

    public static void lingkaran(){
        Scanner input = new Scanner(System.in);
        double phi = 22/7;
        System.out.println("Silahkan masukkan jari - jari : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            lingkaran();
            return;
        }
        double jari2 = input.nextDouble();
        double luaslingkaran = (phi)*(jari2*jari2);
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas lingkaran = %.2f            |\n",luaslingkaran);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }

    public static void segitiga(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan alas : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            segitiga();
            return;
        }
        double panjangST = input.nextDouble();
        System.out.println("Masukkan tinggi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            segitiga();
            return;
        }
        double lebarST = input.nextDouble();
        double luasSegitiga = (panjangST*lebarST)/2;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas Segitiga = %.2f              |\n",luasSegitiga);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }

    public static void persegipanjang(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan panjang : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegipanjang();
            return;
        }
        double panjangPP = input.nextDouble();
        System.out.println("Masukkan lebar : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegipanjang();
            return;
        }
        double lebarPP = input.nextDouble();
        double luasPesrsegiPanjang = panjangPP*lebarPP;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|             Luas persegi Panjang = %.2f       |\n",luasPesrsegiPanjang);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }

}
