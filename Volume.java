package challange_java_ade;
import java.util.Scanner;

public class Volume {

    public static void balok (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan panjang : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double panjangB = input.nextDouble();
        System.out.println("Masukkan lebar : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double lebarB = input.nextDouble();
        System.out.println("Masukkan balok : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double tingggiB = input.nextDouble();
        double volumeBalok = panjangB*lebarB*tingggiB;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Volume Balok = %.2f               |\n",volumeBalok);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }

    public static void kubus (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan sisi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            kubus ();
            return;
        }
        double sisi = input.nextDouble();
        double volumeKubus = sisi*sisi*sisi;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|                 Volume Kubus = %.2f            |\n",volumeKubus);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }

    public static void tabung (){
        Scanner input = new Scanner(System.in);
        double phi = 22/7;
        System.out.println("Masukkan jari-jari : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            tabung ();
            return;
        }
        double jari2T = input.nextDouble();
        System.out.println("Masukkan tinggi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            tabung ();
            return;
        }
        double tingggiT = input.nextDouble();
        double volumeTabung = phi*jari2T*jari2T*tingggiT;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|             Volume Tabung = %.2f             |\n",volumeTabung);
        System.out.println("--------------------------------------------------");
        menu_Calculator.PressAnyKey();
    }
}
