package challange_java_ade;
import java.util.Scanner;

public class calculator_TanpaWhileMain {

    public static void main(String[] args) {
        System.out.println("\t--------------------------------------------------");
        System.out.println("\t|     Kalkulator Penghitung Luas dan Volum       |");
        System.out.println("\t--------------------------------------------------");
        menu_utama();
    }

    public static void menu_utama(){
        Scanner input = new Scanner(System.in);
        menu_utama_dialog();
        String pilihan = input.nextLine();
        switch (pilihan) {
            case "1" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|          Menu Luas Bangun Datar                |");
                System.out.println("--------------------------------------------------");
                sub_menu_luas_pilihan();
            }
            case "2" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|          Menu Luas Bangun Ruang                |");
                System.out.println("--------------------------------------------------");
                sub_menu_volume_pilihan();
            }
            case "3" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|             Mematikan program                  |");
                System.out.println("--------------------------------------------------");
                System.exit(0);
            }
            default -> {
                String kasus = "3";
                salahMasuk(kasus);
            }
        }
        menu_utama();
    }

    public static void menu_utama_dialog(){
        System.out.println("\t--------------------------------------------------");
        System.out.println("\t|               Menu Utama                       |");
        System.out.println("\t--------------------------------------------------");
        System.out.println("\tMenu");
        System.out.println("\t1. Hitung Luas Bidang");
        System.out.println("\t2. Hitung Volum");
        System.out.println("\t3. Tutup Aplikasi");
        System.out.println("\t--------------------------------------------------");
        System.out.print("\tMasukkan angka pilihan menu : ");

    }
    public static void sub_menu_luas_dialog() {
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang datar yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("5. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
        System.out.print("Masukkan angka pilihan menu : ");
    }
    public static void sub_menu_volume_dialog(){
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang ruang yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("4. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
    }

    public static void sub_menu_luas_pilihan(){
        Scanner input = new Scanner(System.in);
        sub_menu_luas_dialog();
        String luaspilih = input.nextLine();
        switch (luaspilih) {
            case "1" -> persegi();
            case "2" -> lingkaran();
            case "3" -> segitiga();
            case "4" -> persegipanjang();
            case "5" -> menu_utama();
            default -> {
                String kasus = "5";
                salahMasuk(kasus);
            }
        }
        sub_menu_luas_pilihan();
    }

    public static void sub_menu_volume_pilihan(){
        Scanner input = new Scanner(System.in);
        sub_menu_volume_dialog();
        String volumepilih = input.nextLine();
        switch (volumepilih) {
            case "1" -> kubus();
            case "2" -> balok();
            case "3" -> tabung();
            case "4" -> menu_utama();
            default -> {
                String kasus = "4";
                salahMasuk(kasus);
            }
        }
        sub_menu_volume_pilihan();
    }

    public static void persegi(){
        Scanner input = new Scanner(System.in);
        System.out.println("Silahkan masukkan sisi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegi();
            return;
        }
        double sisi = input.nextDouble();
        double luasPersegi;
        luasPersegi = sisi*sisi;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas Persegi = %.2f               |\n",luasPersegi);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void lingkaran(){
        Scanner input = new Scanner(System.in);
        double phi = Math.PI;
        System.out.println("Silahkan masukkan jari - jari : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            lingkaran();
            return;
        }
        double jari2 = input.nextDouble();
        double luaslingkaran = (phi)*(jari2*jari2);
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas lingkaran = %.2f            |\n",luaslingkaran);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void segitiga(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan alas : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            segitiga();
            return;
        }
        double panjangST = input.nextDouble();
        System.out.println("Masukkan tinggi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            segitiga();
            return;
        }
        double lebarST = input.nextDouble();
        double luasSegitiga = (panjangST*lebarST)/2;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Luas Segitiga = %.2f              |\n",luasSegitiga);
        System.out.println("--------------------------------------------------");
        System.out.print("Everything on the console will cleared");
        System.out.print("\033[H\033[2J");
        System.out.flush();
        PressAnyKey();
    }

    public static void persegipanjang(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan panjang : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegipanjang();
            return;
        }
        double panjangPP = input.nextDouble();
        System.out.println("Masukkan lebar : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            persegipanjang();
            return;
        }
        double lebarPP = input.nextDouble();
        double luasPesrsegiPanjang = panjangPP*lebarPP;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|             Luas persegi Panjang = %.2f       |\n",luasPesrsegiPanjang);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void balok (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan panjang : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double panjangB = input.nextDouble();
        System.out.println("Masukkan lebar : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double lebarB = input.nextDouble();
        System.out.println("Masukkan balok : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            balok ();
            return;
        }
        double tingggiB = input.nextDouble();
        double volumeBalok = panjangB*lebarB*tingggiB;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|              Volume Balok = %.2f               |\n",volumeBalok);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void kubus (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan sisi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            kubus ();
            return;
        }
        double sisi = input.nextDouble();
        double volumeKubus = sisi*sisi*sisi;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|                 Volume Kubus = %.2f            |\n",volumeKubus);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void tabung (){
        Scanner input = new Scanner(System.in);
        double phi = Math.PI;
        System.out.println("Masukkan jari-jari : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            tabung ();
            return;
        }
        double jari2T = input.nextDouble();
        System.out.println("Masukkan tinggi : ");
        if (!input.hasNextDouble()){ //memastikan input harus decimal, klo gak repeat ke sebelumnya
            System.out.println("--------------------------------------------------");
            System.out.println("|          Anda tidak memasukkan angka           |");
            System.out.println("--------------------------------------------------");
            tabung ();
            return;
        }
        double tingggiT = input.nextDouble();
        double volumeTabung = phi*jari2T*jari2T*tingggiT;
        System.out.println("\nprocessing ... ");
        System.out.println("--------------------------------------------------");
        System.out.printf("|             Volume Tabung = %.2f             |\n",volumeTabung);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void salahMasuk(String angkaPilihan){
        System.out.println("--------------------------------------------------");
        System.out.printf("|       Program hanya menerima masukkan 1 - %s    |\n",angkaPilihan);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }

    public static void PressAnyKey() {
        Scanner input = new Scanner(System.in);
        System.out.println("Press any key to back ...");
        input.nextLine();
        clearScreen();
    }
    public static void clearScreen() {
        /*
         * make the cursor move to the first line and column of the screen and then
         * clear console from the cursor to the end of the screen
         */
        System.out.print("\033[H\033[2J");
        System.out.flush(); // flush the console and make it ready to rewrite
    }

}


/*
metode Lain untuk press anykey
    public static void PressAnyKey() {
bufferedReader membaca karakter dari dengan disimpan dahulu (buffer)
BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try {
            input.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 */



