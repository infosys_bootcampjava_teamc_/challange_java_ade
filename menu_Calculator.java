package challange_java_ade;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class menu_Calculator {

    public static void menu_utama(){
        Scanner input = new Scanner(System.in);
        menu_utama_dialog();
        String pilihan = input.nextLine();
        switch (pilihan) {
            case "1" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|          Menu Luas Bangun Datar                |");
                System.out.println("--------------------------------------------------");
                sub_menu_luas_pilihan();
            }
            case "2" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|          Menu Luas Bangun Ruang                |");
                System.out.println("--------------------------------------------------");
                sub_menu_volume_pilihan();
            }
            case "3" -> {
                System.out.println("--------------------------------------------------");
                System.out.println("|             Mematikan program                  |");
                System.out.println("--------------------------------------------------");
                System.exit(0);
            }
            default -> {
                String kasus = "3";
                salahMasuk(kasus);
            }
        }
        menu_utama();
    }

    public static void menu_utama_dialog(){
        System.out.println("--------------------------------------------------");
        System.out.println("|               Menu Utama                       |");
        System.out.println("--------------------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("3. Tutup Aplikasi");
        System.out.println("--------------------------------------------------");
        System.out.println("Masukkan angka pilihan menu :");

    }
    public static void sub_menu_luas_dialog() {
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang datar yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("5. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
        System.out.println("Masukkan angka pilihan menu :");
    }
    public static void sub_menu_volume_dialog(){
        System.out.println("--------------------------------------------------");
        System.out.println("|        Pilih bidang ruang yang dihtiung        |");
        System.out.println("--------------------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("4. Kembali ke Menu Utama");
        System.out.println("--------------------------------------------------");
    }

    public static void sub_menu_luas_pilihan(){
        Scanner input = new Scanner(System.in);
        sub_menu_luas_dialog();
        String luaspilih = input.nextLine();
        switch (luaspilih) {
            case "1" -> Luas.persegi();
            case "2" -> Luas.lingkaran();
            case "3" -> Luas.segitiga();
            case "4" -> Luas.persegipanjang();
            case "5" -> menu_utama();
            default -> {
                String kasus = "5";
                salahMasuk(kasus);
            }
        }
        sub_menu_luas_pilihan();
    }

    public static void sub_menu_volume_pilihan(){
        Scanner input = new Scanner(System.in);
        sub_menu_volume_dialog();
        String volumepilih = input.nextLine();
        switch (volumepilih) {
            case "1" -> Volume.kubus();
            case "2" -> Volume.balok();
            case "3" -> Volume.tabung();
            case "4" -> menu_utama();
            default -> {
                String kasus = "4";
                salahMasuk(kasus);
            }
        }
        sub_menu_volume_pilihan();
    }

    public static void PressEnter(){
        System.out.println("\nPress enter to continue...");
        try
        {System.in.read();}
        catch(Exception e)
        {e.printStackTrace();}
    }
    public static void salahMasuk(String angkaPilihan){
        System.out.println("--------------------------------------------------");
        System.out.printf("|       Program hanya menerima masukkan 1 - %s    |\n",angkaPilihan);
        System.out.println("--------------------------------------------------");
        PressAnyKey();
    }
    public static void PressAnyKey() {
        Scanner input = new Scanner(System.in);
        System.out.println("Press any key to back ...");
        input.nextLine();
        clearScreen();
    }
    public static void clearScreen() {
        /*
         * make the cursor move to the first line and column of the screen and then
         * clear console from the cursor to the end of the screen
         */
        System.out.print("\033[H\033[2J");
        System.out.flush(); // flush the console and make it ready to rewrite
    }
}
